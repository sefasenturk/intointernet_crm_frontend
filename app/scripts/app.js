'use strict';

/**
 * @ngdoc overview
 * @name intoInternetCrmApp
 * @description
 * # intoInternetCrmApp
 *
 * Main module of the application.
 */
angular.module('intoInternetCrmApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngTouch',
    'config',
    'toastr',
    'angular-jwt',
    'ui.router',
    'mm.iban'
]).config(function ($stateProvider, $urlRouterProvider, USER_ROLES) {

    $urlRouterProvider.when('/', '/login');

    $stateProvider
        .state('login', {
            url: '/login',
            templateUrl: 'views/login.html',
            controller: 'LoginCtrl'
        })

        .state('404', {
            url: '/404',
            name: '404',
            templateUrl: 'views/404.html'
        })

        .state('dashboard', {
            url: '/dashboard',
            templateUrl: 'views/dashboard.html',
            controller: 'DashboardCtrl',
            data: {
                authorizedRoles: [USER_ROLES.admin, USER_ROLES.client, USER_ROLES.orders, USER_ROLES.service]
            }
        })

        .state('clients', {
            url: '/clients',
            templateUrl: 'views/clients.html',
            controller: 'ClientsCtrl',
            data: {
                authorizedRoles: [USER_ROLES.admin, USER_ROLES.client, USER_ROLES.orders, USER_ROLES.service]
            },
            resolve: {
                clientsResolver: function(Client) {
                    return Client.query();
                }
            }
        })

        .state('client', {
            url: '/client/:id',
            templateUrl: 'views/client.html',
            controller: 'ClientCtrl',
            data: {
                authorizedRoles: [USER_ROLES.admin, USER_ROLES.client, USER_ROLES.orders, USER_ROLES.service]
            },
            resolve: {
                clientResolver: function(Client, $stateParams) {
                    if ($stateParams.id == 'new') {
                        return null;
                    }

                    return Client.get({ id: $stateParams.id });
                }
            }
        })

        .state('tickets', {
            url: '/tickets',
            templateUrl: 'views/tickets.html',
            controller: 'TicketsCtrl',
            data: {
                authorizedRoles: [USER_ROLES.admin, USER_ROLES.service]
            },
            resolve: {
                ticketsResolver: function(Ticket) {
                    return Ticket.query();
                }
            }
        })

        .state('orders', {
            url: '/orders',
            templateUrl: 'views/orders.html',
            controller: 'OrdersCtrl',
            data: {
                authorizedRoles: [USER_ROLES.admin, USER_ROLES.client, USER_ROLES.orders, USER_ROLES.service]
            },
            resolve: {
                ordersResolver: function(Order) {
                    return Order.query();
                }
            }
        })

        .state('newOrder', {
            url: '/order/new',
            templateUrl: 'views/order.html',
            controller: 'AddOrderCtrl',
            data: {
                authorizedRoles: [USER_ROLES.admin, USER_ROLES.client, USER_ROLES.orders, USER_ROLES.service]
            },
            resolve: {
                orderResolver: function(Client, Product, $stateParams) {
                    if ($stateParams.id == 'new') {
                        return null;
                    }
                    return [Client.query(),Product.query()];
                }
            }
        })

        .state('order', {
            url: '/order/:id/',
            templateUrl: 'views/viewOrder.html',
            controller: 'OrderCtrl',
            data: {
                authorizedRoles: [USER_ROLES.admin, USER_ROLES.client, USER_ROLES.orders, USER_ROLES.service]
            },
            resolve: {
                orderResolver: function(Order, Product, $stateParams) {
                    if ($stateParams.id == 'new') {
                        return null;
                    }

                    return [Order.get({ id: $stateParams.id }), Product.query()];
                }
            }
        })

        .state('orderClient', {
            url: '/order/new/:clientnumber',
            templateUrl: 'views/order.html',
            controller: 'AddOrderCtrl',
            data: {
                authorizedRoles: [USER_ROLES.admin, USER_ROLES.client, USER_ROLES.orders, USER_ROLES.service]
            },
            resolve: {
                orderResolver: function(Client, Product, $stateParams) {
                    if ($stateParams.id == 'new') {
                        return null;
                    }
                    return [Client.query(),Product.query()];
                }
            }
        })

        .state('products', {
            url: '/products',
            templateUrl: 'views/products.html',
            controller: 'ProductsCtrl',
            data: {
                authorizedRoles: [USER_ROLES.admin, USER_ROLES.client, USER_ROLES.orders, USER_ROLES.service]
            },
            resolve: {
                productsResolver: function(Product) {
                    return Product.query();
                }
            }
        })

        .state('product', {
            url: '/product/:id',
            templateUrl: 'views/product.html',
            controller: 'ProductCtrl',
            data: {
                authorizedRoles: [USER_ROLES.admin, USER_ROLES.client, USER_ROLES.orders, USER_ROLES.service]
            },
            resolve: {
                productResolver: function(Product, $stateParams) {
                    if ($stateParams.id == 'new') {
                        return null;
                    }

                    return Product.get({ id: $stateParams.id });
                }
            }
        });

    $urlRouterProvider.otherwise("/dashboard");

}).run( function($rootScope, AuthService, $state, AUTH_EVENTS) {

    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams, options) {

        if ('data' in toState && 'authorizedRoles' in toState.data) {
            var authorizedRoles = toState.data.authorizedRoles;

            if (!AuthService.isAuthorized(authorizedRoles)) {

                console.log('NOT AUTHORIZED');
                $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
                event.preventDefault();

                $state.go('404');
            }
        }


        if (AuthService.isAuthenticated()) {
            if (toState.name === 'login') {
                event.preventDefault();
                $state.go('dashboard');
            }
        } else {
            if (toState.name !== 'login') {
                event.preventDefault();
                $state.go('login');
            }
        }

    });
});
