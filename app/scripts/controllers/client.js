'use strict';

/**
 * @ngdoc function
 * @name intoInternetCrmApp.controller:ClientCtrl
 * @description
 * # ClientCtrl
 * Controller of the intoInternetCrmApp
 */
angular.module('intoInternetCrmApp').controller('ClientCtrl', function ($stateParams, $scope, clientResolver, Client, toastr, $state, Contactperson) {

    $scope.readOnly = true;
    $scope.new = false;

    if (clientResolver == null) {
        $scope.client = {
            clientnumber: null
        };

        $scope.new = true;

        $('input[readonly]').removeAttr('readonly');
    } else {
        $scope.client = clientResolver;
    }

    $('#editContactpersonModal').on('hidden.bs.modal', function() {
        if ($scope.readOnly) {
            $('#editContactpersonModal input').attr('readonly', 'readonly');
        }

        $scope.contactperson = {};
    });

    $scope.edit = function() {
        $('input[readonly]').removeAttr('readonly');
        $scope.readOnly = false;
    };

    $scope.save = function() {
        if ($scope.bankForm.$valid && $scope.clientForm.$valid && $scope.companyForm.$valid) {
            var saveButton = $('#btnSaveClient');
            saveButton.button('loading');

            console.log('form is valid');

            var user = {
                username: $scope.client.username,
                password: $scope.client.password
            };

            var address = {
                street: $scope.client.street,
                housenumber: $scope.client.housenumber,
                extension: $scope.client.extension,
                zip: $scope.client.zip,
                city: $scope.client.city,
                email: $scope.client.email,
                phonenumber: $scope.client.phonenumber
            };

            var client = {
                name: $scope.client.name,
                iban: $scope.client.iban.replace(/\s+/g, ''),
                bic: $scope.client.bic,
                coc: $scope.client.coc
            };


            if ($scope.client.clientnumber === null) {
                Client.save({
                    user: user,
                    address: address,
                    client: client,
                    contactperson: $scope.client.contactperson
                }, function (result) {

                    if (result.success) {
                        toastr.success('De klant is succesvol aangemaakt.');
                        $state.go('clients');
                    }

                    console.log(result);
                    saveButton.button('reset');

                }, function (err) {

                    toastr.error('Er is wat fout gegaan.. Probeer opnieuw.');
                    console.log(err);
                    saveButton.button('reset');
                });
            } else {


                address.addressnumber = $scope.client.addressnumber;

                Client.update({ id: $scope.client.clientnumber }, {
                    user: user,
                    address: address,
                    client: client
                }, function (result) {
                    console.log(result);

                    if (result) {
                        toastr.success('De contactpersoon is succesvol gewijzigd.');
                        saveButton.button('reset');
                    }

                }, function (err) {
                    console.log(err);
                    saveButton.button('reset');
                    toastr.error('Er is wat fout gegaan.. Probeer opnieuw.');
                });
            }
        } else {
            console.log('form not valid');
        }
    };

    $scope.viewContactperson = function(cp) {
        $scope.contactperson = cp;
        $('#editContactpersonModal').modal('show');
    };

    $scope.newContactperson = function() {
        $scope.contactperson = {};
        $('#editContactpersonModal input[readonly]').removeAttr('readonly');
        $('#editContactpersonModal').modal('show');
    };

    $scope.saveContactperson = function() {

        if ($scope.contactpersonForm.$valid) {
            var saveButton = $('#btnSaveContactperson');

            console.log('its valid');

            saveButton.button('loading');

            var address = {
                street: $scope.contactperson.street,
                housenumber: $scope.contactperson.housenumber,
                extension: $scope.contactperson.extension,
                zip: $scope.contactperson.zip,
                city: $scope.contactperson.city,
                email: $scope.contactperson.email,
                phonenumber: $scope.contactperson.phonenumber
            };

            var contactperson = {
                title: $scope.contactperson.title,
                role: $scope.contactperson.role,
                initials: $scope.contactperson.initials,
                firstname: $scope.contactperson.firstname,
                lastname: $scope.contactperson.lastname,
                companyname: $scope.contactperson.companyname
            };

            if ($scope.contactperson.contactpersonnumber === undefined) {
                Contactperson.save({
                    address: address,
                    contactperson: contactperson,
                    clientnumber: $stateParams.id
                }, function (result) {
                    console.log(result);

                    if (result) {
                        toastr.success('De contactpersoon is succesvol toegevoegd.');
                        $scope.client = Client.get({id: $stateParams.id});

                        saveButton.button('reset');
                        $('#editContactpersonModal').modal('hide');
                    }

                }, function (err) {
                    console.log(err);
                    toastr.error('Er is wat fout gegaan.. Probeer opnieuw.');
                    saveButton.button('reset');
                });
            } else {
                console.log($scope.contactperson);

                Contactperson.update({ id: $scope.contactperson.contactpersonnumber }, {
                    address: address,
                    contactperson: contactperson,
                    addressnumber: $scope.contactperson.addressnumber
                }, function (result) {
                    console.log(result);

                    if (result) {
                        toastr.success('De contactpersoon is succesvol gewijzigd.');
                        saveButton.button('reset');
                        $('#editContactpersonModal').modal('hide');
                    }

                }, function (err) {
                    console.log(err);
                    saveButton.button('reset');
                    toastr.error('Er is wat fout gegaan.. Probeer opnieuw.');
                });
            }
        } else {
            console.log('its not valid');
        }
    };

    $scope.removeContactperson = function() {
        var removeButton = $('#btnRemoveContactperson');

        if (confirm('Weet je zeker dat je deze contactpersoon wilt verwijderen?')) {

            removeButton.button('loading');

            Contactperson.delete({ id: $scope.contactperson.contactpersonnumber, clientId: $scope.client.clientnumber}, {  }, function(result) {
                console.log(result);

                if (result.success) {
                    $scope.client = Client.get({id: $stateParams.id});
                    toastr.success('De contactpersoon is succesvol verwijderd.');
                    removeButton.button('reset');
                    $('#editContactpersonModal').modal('hide');
                }
            }, function(err) {
                console.log(err);
                toastr.error('Er is wat fout gegaan.. Probeer opnieuw.');
                removeButton.button('reset');
            });
        }
    };
});
