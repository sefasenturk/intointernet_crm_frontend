'use strict';

/**
 * @ngdoc function
 * @name intoInternetCrmApp.controller:ClientsCtrl
 * @description
 * # ClientsCtrl
 * Controller of the intoInternetCrmApp
 */
angular.module('intoInternetCrmApp').controller('ClientsCtrl', function ($scope, clientsResolver) {

    $scope.clients = clientsResolver;

});
