'use strict';

/**
 * @ngdoc function
 * @name intoInternetCrmApp.controller:TicketsCtrl
 * @description
 * # TicketsCtrl
 * Controller of the intoInternetCrmApp
 */
angular.module('intoInternetCrmApp').controller('TicketsCtrl', function ($scope, ticketsResolver) {
    $scope.tickets = ticketsResolver;
});
