'use strict';
/**
 * @ngdoc function
 * @name intoInternetCrmApp.controller:ProductsCtrl
 * @description
 * # ProductsCtrl
 * Controller of the intoInternetCrmApp
 */
angular.module('intoInternetCrmApp').controller('ProductCtrl',function ($scope, productResolver, $state, Product, toastr, $stateParams, BTW) {

    $scope.readOnly = true;
    $scope.new = false;

    if (productResolver == null) {
        $('input[readonly]').removeAttr('readonly');
        $('textarea[readonly]').removeAttr('readonly');
        $('#vat').removeAttr('disabled');

        $scope.product = {
            productnumber: null,
        };

        $scope.new = true;

    } else {
        $scope.product = productResolver;
    }

    $scope.getBTW = BTW.query();

    $scope.edit = function() {
        $('input[readonly]').removeAttr('readonly');
        $('textarea[readonly]').removeAttr('readonly');
        $('#vat').removeAttr('disabled');
        $scope.readOnly = false;
    };

    $scope.save = function() {
        if ($scope.productForm.$valid){
            var saveButton = $('#btnSaveClient');
            saveButton.button('loading');

            console.log($scope.product.btw);
            var product = {
                name: $scope.product.name,
                price: $scope.product.price,
                vat: $scope.product.btw,
                description: $scope.product.description,
                longdescription: $scope.product.longdescription
            };

            if ($scope.product.productnumber === null){
                Product.save({
                    product:product
                }, function(result) {

                    if (result.success) {
                        toastr.success('Het product is succesvol aangemaakt.');
                        $state.go('products');
                    }
                    console.log(result);

                }, function(err) {

                    toastr.error('Er is wat fout gegaan.. Probeer opnieuw.');
                    console.log(err);

                });
            } else {
                product.productnumber = $scope.product.productnumber;

                Product.update({ id: $scope.product.productnumber},{
                    product:product
                }, function (result) {
                    console.log(result);

                    if (result) {
                        toastr.success('Het product is succesvol gewijzigd.');
                        saveButton.button('reset');
                    }

                }, function (err) {
                    console.log(err);
                    saveButton.button('reset');
                    toastr.error('Er is wat fout gegaan.. Probeer opnieuw.');
                });
            }
        } else {
            toastr.error('Formulier incorrect ingevuld');
        }

    };

    $scope.delete = function(){
        if(confirm("Weet u zeker dat u dit product wilt verwijderen?")){
            Product.delete({ id:$stateParams.id}, function(result){
                if (result){
                    toastr.success('Het product is succesvol verwijdert.');
                    $state.go('products');
                }
            }, function(err){
                console.log(err);
                toastr.error('Er is wat fout gegaan... Probeer opnieuw.');
            });
        }
    };

});

