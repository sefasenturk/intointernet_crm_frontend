'use strict';

/**
 * @ngdoc function
 * @name intoInternetCrmApp.controller:OrdersCtrl
 * @description
 * # OrdersCtrl
 * Controller of the intoInternetCrmApp
 */
angular.module('intoInternetCrmApp').controller('OrdersCtrl', function (AuthService, $scope, ordersResolver) {
    $scope.user = AuthService.user();
    $scope.orders = ordersResolver;
});
