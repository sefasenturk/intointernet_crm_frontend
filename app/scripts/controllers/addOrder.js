'use strict';

/**
 * @ngdoc function
 * @name intoInternetCrmApp.controller:AddOrderCtrl
 * @description
 * # AddOrderCtrl
 * Controller of the intoInternetCrmApp
 */
angular.module('intoInternetCrmApp').controller('AddOrderCtrl', function ($stateParams,$scope, orderResolver, Order, toastr, $state) {
    $scope.order = {};
    $scope.order.clientNumber=$stateParams.clientnumber;
    $scope.order.billingInterval = '1m';
    $scope.clients = orderResolver[0];
    $scope.products = orderResolver[1];
    $scope.inputs=[{"number":0,"productnumber":"NaN","discount":0}];

    $scope.addProduct = function() {
        $scope.inputs.push({"number":$scope.inputs[$scope.inputs.length-1].number+1,"productnumber":"NaN","discount":0});
    };

    $scope.verifyDuplicate = function(newProduct) {
        for(var i = 0; i<$scope.inputs.length; i++){
            if(newProduct.productnumber == $scope.inputs[i].productnumber && newProduct.number != $scope.inputs[i].number){
                toastr.error("Er kan niet tweemaal hetzelfde product worden ingevoerd.");
                return false;
            }
        }
    };

    $scope.removeProduct = function(input) {
        $scope.inputs.splice($scope.inputs.indexOf(input), 1);
    };

    $scope.submitOrder = function(order){
        if(verifyData()) {

            // console.log($scope.order.planningDate);
            // console.log($scope.order.billingDate);

            $scope.order.appointment.date = new Date($scope.order.appointment.date);
            $scope.order.billingDate = new Date($scope.order.billingDate);
            $scope.order.appointment.date.setTime($scope.order.appointment.date.getTime() + (14400 * 1000));
            $scope.order.billingDate.setTime($scope.order.billingDate.getTime() + (14400 * 1000));
            $scope.order.appointment.estimation = $scope.order.appointment.estimation.toTimeString().slice(0,8);
            toastr.error($scope.order.appointment.estimation);

            // console.log($scope.order.planningDate);
            // console.log($scope.order.billingDate);

            Order.save({
                order: order,
                products: $scope.inputs
            }, function (result) {
                if (result.success) {
                    toastr.success('De order is succesvol aangemaakt.');
                    $state.go('clients');
                }
                console.log(result);
            }, function (err) {
                toastr.error('Er is wat fout gegaan.. Probeer opnieuw.');
                console.log(err);
            });
        }
        else{
            toastr.error('Er staat een fout in de gegevens.. Probeer opnieuw.');
        }
    };

    var verifyData = function(){
        for(var i = 0; i<$scope.inputs.length; i++){
            if($scope.inputs[i].productnumber == "NaN"){
                return false;
            }
            else if($scope.verifyDuplicate($scope.inputs[i]) == false){
                return false;
            }
            else{
                return true;
            }
        }
    }
});
