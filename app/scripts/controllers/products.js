'use strict';

/**
 * @ngdoc function
 * @name intoInternetCrmApp.controller:ProductsCtrl
 * @description
 * # ProductsCtrl
 * Controller of the intoInternetCrmApp
 */
angular.module('intoInternetCrmApp').controller('ProductsCtrl', function ($scope, productsResolver) {
    $scope.products = productsResolver;
});
