'use strict';

/**
 * @ngdoc function
 * @name intoInternetCrmApp.controller:DashboardCtrl
 * @description
 * # DashboardCtrl
 * Controller of the intoInternetCrmApp
 */
angular.module('intoInternetCrmApp').controller('DashboardCtrl', function (AuthService, $scope) {

    $scope.user = AuthService.user();

});
