'use strict';

/**
 * @ngdoc function
 * @name intoInternetCrmApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the intoInternetCrmApp
 */
angular.module('intoInternetCrmApp').controller('LoginCtrl', function ($scope, AuthService, toastr, $location) {

    $scope.authenticate = function() {
        AuthService.authenticate($scope.username, $scope.password).then(function(result) {
            console.log(result);

            if (!result) {
                return toastr.error('De gegevens waarmee u probeerde in te loggen zijn onjuist.');
            }

            console.log('Redirecting to dashboard..');
            $location.path('/dashboard');
        });
    };

});
