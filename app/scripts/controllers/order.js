'use strict';
/**
 * @ngdoc function
 * @name intoInternetCrmApp.controller:VierOrderCtrl
 * @description
 * # ViewOrderCtrl
 * Controller of the intoInternetCrmApp
 */
angular.module('intoInternetCrmApp').controller('OrderCtrl', function ($scope, orderResolver, Order, ConfigurationSettings, toastr, $state, Invoice) {
    $scope.order = orderResolver[0];
    $scope.products = orderResolver[1];
    $scope.readOnly = true;
    $scope.readConfigSettingsOnly = true;
    $scope.totalPrice = 0.00;
    $scope.totalDiscount = 0.00;
    $scope.configSettings = null;


    $scope.getInvoice = function() {
        Invoice.get({ orderId: $scope.order.ordernumber}).$promise.then(function(result) {
            console.log(result);
            downloadURI("http://maatwerk.focusws.nl/intointernet-invoice/" + result.invoiceURL, 'invoice.pdf');
        });
    };

    $scope.viewConfig = function(product) {
        $scope.selectedProduct = product;
        // $scope.configSettings = ConfigurationSettings.get({ oid: $scope.order.ordernumber , pid: product.productnumber });
        $('#productModal').modal('show');
    };

    $scope.order.$promise.then(function() {
        // $scope.billingdate = new Date($scope.order.billingdate);
        if ($scope.order.products !== undefined) {
            for (var index in $scope.order.products) {
                $scope.totalPrice += $scope.order.products[index].price;
                $scope.totalDiscount += $scope.order.products[index].discount;
            }
        }
    });

    $scope.editOrder = function() {
        console.log($scope.order.invoice.mayEdit);
        if($scope.order.invoice.mayEdit == 0) {
            $scope.readOnly = false;
        } else {
            toastr.error("Deze order mag niet gewijzigd worden")
        }
    };

    $scope.saveOrder = function() {
        $scope.readOnly = true;

        // $scope.billingdate = new Date($scope.billingdate);
        // $scope.billingdate.setTime($scope.billingdate.getTime() + (14400 * 1000));
        //
        // Order.update({ id : $scope.order.ordernumber}, {id : $scope.order.ordernumber, date : $scope.billingdate }, function(result) {
        //     if (result.success) {
        //         toastr.success('De order is succesvol gewijzigd');
        //     }
        // }, function(err) {
        //
        //     toastr.error('Er is wat fout gegaan.. Probeer opnieuw.');
        //     console.log(err);
        //
        // });
    };

    $scope.saveProduct = function () {
        toastr.error("Deze functie is nog niet beschikbaar");
    };

    $scope.savePlanning = function() {
        toastr.error("Deze functie is nog niet beschikbaar");
    };

    $scope.deleteOrder = function () {
        if (confirm("Weet u zeker dat u deze order wilt verwijderen?")) {
            Order.delete({id: $scope.order.ordernumber}, function (result) {
                if (result.success) {
                    toastr.success('De order is succesvol verwijderd');
                    $state.go('orders');
                }
                console.log(result);
            }, function (err) {
                toastr.error('Er is wat fout gegaan.. Probeer opnieuw.');
                console.log(err);
            });
        }
    };

    $scope.edit = function() {
        $('input[readonly]').removeAttr('readonly');
        $scope.readOnly = false;
    };

    $scope.editConfigSettings = function() {
        $('input[readConfigSettingsOnly]').removeAttr('readConfigSettingsOnly');
        $scope.readConfigSettingsOnly = false;
    };

    $scope.saveConfigurationSettings = function(productnumber) {
        if ($scope.checkInputs($scope.selectedProduct.configSettings, productnumber) == false) {
            ConfigurationSettings.delete({
                    oid: $scope.order.ordernumber,
                    pid: productnumber
                }, {ordernumber: $scope.order.ordernumber, productnumber: productnumber},

                function (result) {
                    if (result.success) {
                        var saveButton = $('#btnSaveConfigDetails');
                        saveButton.button('loading');
                        $scope.readConfigSettingsOnly = true;

                        console.log('Saving Config Details');

                        ConfigurationSettings.save({
                            ordernumber: $scope.order.ordernumber,
                            productnumber: productnumber,
                            configSettings: $scope.selectedProduct.configSettings
                        }, function (result) {
                            if (result.success) {
                                toastr.success('De configuratiegegevens zijn succesvol opgeslagen.');
                                saveButton.button('reset');
                            }
                            console.log(result);
                        }, function (err) {
                            toastr.error('Er is wat fout gegaan.. Probeer opnieuw.');
                            console.log(err);
                            saveButton.button('reset');
                        });
                    }
                    console.log(result);
                }, function (err) {
                    toastr.error('Er is wat fout gegaan.. Probeer opnieuw.');
                    console.log(err);
                });
        }
    };

    $scope.deleteConfigSettings = function (configSetting) {
        var array = $scope.selectedProduct.configSettings;
        array.splice(array.indexOf(configSetting), 1);
    };

    $scope.addConfigRow = function() {
        $scope.selectedProduct.configSettings.push({
            configname: '',
            configdescription: ''
        });
    };

    $scope.close = function() {
        $scope.readOnly = true;
    };

    $scope.closeConfigSettings = function() {
        $scope.readConfigSettingsOnly = true;
    };

    $scope.checkInputs = function(configSettings) {
        var emptyInput = false;
        configSettings.forEach(function(configSetting) {
            if (configSetting.configname == '' || configSetting.configdescription == '') {
                toastr.error('Niet alle velden zijn ingevuld.');
                emptyInput = true;
                return emptyInput;
            }

            configSettings.forEach(function(duplicate) {
                if ((configSetting.configname == duplicate.configname) && (configSettings.indexOf(configSetting) != configSettings.indexOf(duplicate)) && !emptyInput) {
                    toastr.error('Niet alle velden zijn uniek.');
                    emptyInput = true;
                    return emptyInput;
                }
            });
        });
        return emptyInput;
    };
});

function downloadURI(uri, name) {
    var link = document.createElement("a");
    link.download = name;
    link.href = uri;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);

    $(link).remove();
}
