'use strict';

/**
 * @ngdoc service
 * @name intoInternetCrmApp.TicketService
 * @description
 * # TicketService
 * Service in the intoInternetCrmApp.
 */
angular.module('intoInternetCrmApp').service('Ticket', function (ENV, $resource) {
    return $resource(ENV.apiEndpoint + 'tickets/:id', {}, {
        'update': {
            method: 'PUT'
        }
    });
});
