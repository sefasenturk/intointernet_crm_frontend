'use strict';

/**
 * @ngdoc service
 * @name intoInternetCrmApp.contactperson
 * @description
 * # contactperson
 * Service in the intoInternetCrmApp.
 */
angular.module('intoInternetCrmApp').service('Contactperson', function ($resource, ENV) {
    return $resource(ENV.apiEndpoint + 'contactpersons/:id/:clientId', {}, {
        'update': {
            method: 'PUT'
        }
    });
});
