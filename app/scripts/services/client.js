'use strict';

/**
 * @ngdoc service
 * @name intoInternetCrmApp.clients
 * @description
 * # clients
 * Service in the intoInternetCrmApp.
 */
angular.module('intoInternetCrmApp').service('Client', function (ENV, $resource) {
    return $resource(ENV.apiEndpoint + 'clients/:id', {}, {
        'update': {
            method: 'PUT'
        }
    });
});
