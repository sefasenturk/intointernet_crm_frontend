'use strict';

/**
 * @ngdoc service
 * @name intoInternetCrmApp.USER_ROLES
 * @description
 * # USER_ROLES
 * Constant in the intoInternetCrmApp.
 */
angular.module('intoInternetCrmApp').constant('USER_ROLES', {
    admin: 'admin_role',
    client: 'client_role',
    orders: 'orders_role',
    service: 'service_role'
});
