'use strict';

/**
 * @ngdoc service
 * @name intoInternetCrmApp.AUTH_EVENTS
 * @description
 * # AUTH_EVENTS
 * Constant in the intoInternetCrmApp.
 */
angular.module('intoInternetCrmApp').constant('AUTH_EVENTS', {
      notAuthenticated: 'auth-not-authenticated',
      notAuthorized: 'auth-not-authorized'
});
