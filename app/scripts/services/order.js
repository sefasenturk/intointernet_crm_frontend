'use strict';

/**
 * @ngdoc service
 * @name intoInternetCrmApp.OrderService
 * @description
 * # OrderService
 * Service in the intoInternetCrmApp.
 */
angular.module('intoInternetCrmApp').service('Order', function (ENV, $resource) {
    return $resource(ENV.apiEndpoint + 'orders/:id', {}, {
        'update': {
            method: 'PUT'
        }
    });
});
