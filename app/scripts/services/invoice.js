'use strict';

/**
 * @ngdoc service
 * @name intoInternetCrmApp.invoice
 * @description
 * # invoice
 * Service in the intoInternetCrmApp.
 */
angular.module('intoInternetCrmApp').service('Invoice', function ($resource, ENV) {
    return $resource(ENV.apiEndpoint + 'invoices/:orderId', {}, {});
});
