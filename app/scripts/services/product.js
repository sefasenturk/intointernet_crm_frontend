'use strict';

/**
 * @ngdoc service
 * @name intoInternetCrmApp.ProductService
 * @description
 * # ProductService
 * Service in the intoInternetCrmApp.
 */
angular.module('intoInternetCrmApp').service('Product', function (ENV, $resource) {
    return $resource(ENV.apiEndpoint + 'products/:id', {}, {
        'update': {
            method: 'PUT'
        },
        'delete': {
            method: 'DELETE'
        }
    })
});
