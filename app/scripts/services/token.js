'use strict';

/**
 * @ngdoc service
 * @name intoInternetCrmApp.Token
 * @description
 * # Token
 * Factory in the intoInternetCrmApp.
 */
angular.module('intoInternetCrmApp').factory('Token', function(ENV, $resource) {
    return $resource(ENV.apiEndpoint + 'tokens', {},
        {
            Request: {
                method: 'POST'
            }
        }
    );
});
