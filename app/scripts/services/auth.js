'use strict';

/**
 * @ngdoc service
 * @name intoInternetCrmApp.AuthService
 * @description
 * # AuthService
 * Service in the intoInternetCrmApp.
 */
angular.module('intoInternetCrmApp').service('AuthService', function ($cookies, Token, $q, jwtHelper, USER_ROLES, $http, $rootScope, $state) {

    var user = {
        username: null,
        role: null
    };

    $rootScope.isAuthenticated = false;
    var authToken;

    function loadUserCredentials() {
        var token = $cookies.get('token');

        console.log(token);

        if (token) {
            useCredentials(token);
        }
    }

    function storeUserCredentials(token) {
        $cookies.put('token', token);
        useCredentials(token);
    }

    function useCredentials(token) {

        var data = jwtHelper.decodeToken(token);

        if (jwtHelper.isTokenExpired(token)) {
            $cookies.remove('token');

            return $state.go('login');
        }

        user.username = data.sub;

        if (data.role === 'client') {
            user.role = USER_ROLES.client;
        } else if (data.role === 'admin') {
            user.role = USER_ROLES.admin;
        } else if (data.role === 'orders') {
            user.role = USER_ROLES.orders;
        } else {
            user.role = USER_ROLES.service;
        }

        $rootScope.isAuthenticated = true;
        authToken = token;

        $http.defaults.headers.common.Authorization = 'Bearer ' + authToken;

    }

    function destroyUserCredentials() {
        authToken = undefined;

        user = {
            username: null,
            role: null
        };

        $rootScope.isAuthenticated = false;
        $http.defaults.headers.common.Authorization = undefined;
        $cookies.remove('token');
    }

    var authenticate = function(name, pw) {

        var deferred = $q.defer();

        Token.Request({
            username: name,
            password: pw
        }).$promise.then(function(res) {

            storeUserCredentials(res.token);

            deferred.resolve(true);

        }, function(err) {

            if (err.status === 401) {
                deferred.resolve(false);
            }
        });


        return deferred.promise;
    };

    var logout = function() {
        destroyUserCredentials();
    };

    var isAuthorized = function(authorizedRoles) {
        if (!angular.isArray(authorizedRoles)) {
            authorizedRoles = [authorizedRoles];
        }
        return ($rootScope.isAuthenticated && authorizedRoles.indexOf(user.role) !== -1);
    };

    loadUserCredentials();

    return {
        authenticate: authenticate,
        logout: logout,
        isAuthorized: isAuthorized,
        isAuthenticated: function() {
            return $rootScope.isAuthenticated;
        },
        user: function() {
            return user;
        }
    };

});
