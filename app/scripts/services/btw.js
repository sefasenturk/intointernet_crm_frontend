'use strict';

/**
 * @ngdoc service
 * @name intoInternetCrmApp.btw
 * @description
 * # btw
 * Service in the intoInternetCrmApp.
 */
angular.module('intoInternetCrmApp').service('BTW', function (ENV, $resource) {
    return $resource(ENV.apiEndpoint + 'btw/:id', {}, {
        'update': {
            method: 'PUT'
        },
        'delete': {
            method: 'DELETE'
        }
    });
});
