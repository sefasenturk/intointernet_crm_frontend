'use strict';

/**
 * @ngdoc service
 * @name intoInternetCrmApp.ConfigurationSettings
 * @description
 * # ConfigurationSettingsService
 * Service in the intoInternetCrmApp.
 */

angular.module('intoInternetCrmApp').service('ConfigurationSettings', function (ENV, $resource) {
    return $resource(ENV.apiEndpoint + 'configurationSettings/:oid/:pid', {}, {
        'update': {
            method: 'PUT'
        }
    });
});
