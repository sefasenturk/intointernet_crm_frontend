'use strict';

/**
 * @ngdoc directive
 * @name intoInternetCrmApp.directive:headerMenu
 * @description
 * # headerMenu
 */
angular.module('intoInternetCrmApp').directive('headerMenu', function (AuthService, USER_ROLES,  $location) {
    return {
        templateUrl: 'scripts/directives/headermenu/headermenu.html',
        restrict: 'E',
        link: function(scope, elem, attr) {

            scope.username = AuthService.user().username;
            scope.userrole = AuthService.user().role;
            scope.roles = USER_ROLES;

            scope.logOut = function() {
                AuthService.logout();
                $location.path('/login');
            }

        }
    };
});
