'use strict';

/**
 * @ngdoc directive
 * @name intoInternetCrmApp.directive:footerPart
 * @description
 * # footerPart
 */
angular.module('intoInternetCrmApp').directive('footerPart', function () {
    return {
        templateUrl: 'scripts/directives/footerpart/footerpart.html',
        restrict: 'E'
    };
});
