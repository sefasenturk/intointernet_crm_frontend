'use strict';

describe('Service: contactperson', function () {

  // load the service's module
  beforeEach(module('intoInternetCrmApp'));

  // instantiate service
  var contactperson;
  beforeEach(inject(function (_contactperson_) {
    contactperson = _contactperson_;
  }));

  it('should do something', function () {
    expect(!!contactperson).toBe(true);
  });

});
